<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $table = 'ch_messages';

    public function user()
    {
        return $this->belongsTo('App\Models\User','to_id');
    }
}
