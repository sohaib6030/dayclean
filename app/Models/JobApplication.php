<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class JobApplication extends Model
{
    use HasFactory;

    protected $table = 'job_applications';

    protected $fillable = ['job_id','user_id','reviews','ratings','invoice','reference','status','last_updated_by','created_by'];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }
}
