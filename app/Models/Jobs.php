<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\PostJobCategory;
use App\Models\JobApplication;

class Jobs extends Model
{
    use HasFactory;

    protected $fillable = [
       'description' ,'address' ,'offer' ,'posted_by' ,'category_id' ,'date' ,'time'
    ];

    public function postedBy()
    {
        return $this->hasOne(User::class,'id','posted_by')->withDefault();
    }
    
    
    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id')
                            ->select('category.*');
    }
    
    public function subcategories(){
        return $this->hasMany(PostJobCategory::class, 'job_id')
                    ->join('subcategory', 'subcategory.id', '=', 'post_job_category.sub_category_id')
                    ->select('subcategory.*', 'post_job_category.job_id AS post_job_id');
                    
    }

    public function applicantDetails(){
        return $this->hasOne(JobApplication::class,'job_id')->where('status' ,'active');
    }

    public function jobApplicationCount(){
        return $this->hasMany(JobApplication::class,'job_id');
    }
}
