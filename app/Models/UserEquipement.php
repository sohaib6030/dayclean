<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEquipement extends Model
{
    use HasFactory;
    
    public $timestamps = false;

    protected $table = 'user_equipments';

    protected $fillable = ['user_id','name','status'];
}
