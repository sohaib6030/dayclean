<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubCategory extends Model
{
    use HasFactory;

    protected $table = 'user_subcategory';

    protected $fillable = ['id','user_id','sub_category_id','name','status'];
}
