<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostJobCategory extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $table = 'post_job_category';

    protected $fillable = ['job_id','sub_category_id'];
}
