<?php

namespace App\Http\Helpers;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\User;

class Helper
{

    public static function getFormattedDate($date){

       return $date =  Carbon::createFromFormat('Y-m-d',$date)->format('D M d Y');
    }

    public static function quickRandom($length = 16)
    {
        $random = Str::random($length);
        return $random;
    }

    public static function sendEmailForgot($user,$code)
    {

        $url = url('resetpassword/' . $user->email . '/' . urlencode($code));
        $email_a = trim($user->email);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            //  $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host =env("MAIL_HOST","n/a");                // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username =env("MAIL_USERNAME",'n/a');                     // SMTP username
            $mail->Password =env("MAIL_PASSWORD","n/a");                              // SMTP password
            $mail->SMTPSecure =env("MAIL_ENCRYPTION","n/a");          // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 465;                                   // TCP port to connect to

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom(env("MAIL_FROMADDRESS","n/a"), 'EVNGO');
            //  $mail->addAddress($email_a, $user->fname  );     // Add a recipient
            $mail->addAddress($email_a, $user->fname);     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Hello " . $user->fname . " Reset Your Password";
            //  $mail->Body    = "test";
            $mail->Body = "<h2>Reset Your Password</h2><p>To change your password <a href='" . $url . "'>click here.</a></p><p>Or point your browser to this address: <br />" . $url . "</p> Thank you!</p>";

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }


    }








    public static function sendEmail($data,$code)
    {

        $url = url('verify/account/'.$data->email.'/'.urlencode($code));
        $email_a = trim($data->email);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            //   $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host =env("MAIL_HOST","n/a");                // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username =env("MAIL_USERNAME",'n/a');                     // SMTP username
            $mail->Password =env("MAIL_PASSWORD","n/a");                              // SMTP password
            $mail->SMTPSecure =env("MAIL_ENCRYPTION","n/a");          // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom(env("MAIL_FROMADDRESS","n/a"), 'EVNGO');
            //  $mail->addAddress($email_a, $user->fname  );     // Add a recipient
            $mail->addAddress($email_a, $data->fname);     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Hello " . $data->fname . " verify your account";
            //  $mail->Body    = "test";
            $mail->Body = "<h2>Verify Your Account</h2><p>To verify your account,<a href=".$url.">click here.</a></p><p>Or point your browser to this address: <br />".$url."</p><p>Thank you!</p>";

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }



    public static function sendCustomEmail($data,$type,$code){
        require base_path("vendor/autoload.php");
        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host =env("MAIL_HOST","n/a");                // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username =env("MAIL_USERNAME",'n/a');                     // SMTP username
            $mail->Password =env("MAIL_PASSWORD","n/a");                              // SMTP password
            $mail->SMTPSecure =env("MAIL_ENCRYPTION","n/a");         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 465;// TCP port to connect to
            $mail->Priority = 1;
            $mail->AddCustomHeader("X-MSMail-Priority: High");
            $mail->WordWrap = 50; 
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom(env("MAIL_FROMADDRESS","n/a"), 'EVNGO');
            //  $mail->addAddress($email_a, $user->fname  );     // Add a recipient
            //verify account 
            if($type == 'verify_account'){
                $mail->addAddress(trim($data->email), $data->fname  );
                $mail->Subject = "Hello " . $data->fname . " verify your account"; 
            }
            //forgotpassword
            if($type == 'forgotpassword'){
                $mail->addAddress(trim($data->email), $data->fname  ); 
                $mail->Subject = "Hello " . $data->fname . " Reset Your Password" ;
            }
            // onboarding verified account 
            if($type == "onboarding"){
                $mail->addAddress($data->email, $data->fname); 
                $mail->Subject = "Hello " . $data->fname . " Your Account Has Been Verified";
            }
            //report user 
            if($type == 'report_user'){
                $mail->Subject = "Report Recieved";
                $users = User::where('role_id', 1)->get();
                foreach ($users as $user) {
                    $mail->addAddress($user->email, $user->fname . " " . $user->lname); //Add Multiple Reciepents
                }
            }
           

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
           
            $mail->Body = Self::getEmailBodyTypes($data,$type,$code);
            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getEmailBodyTypes($data,$type,$code){
        if($type=='verify_account'){
            $url = url('verify/accounts/'.$data->email.'/'.urlencode($code));
            $email_a = trim($data->email);
            return "<h2>Verify Your Account</h2><p>To verify your account,<a href='".$url."' >click here</a></p><p>Or point your browser to this address: <br />".$url."</p><p>Thank you!</p>";
        }
        if($type=='forgotpassword'){
            $url = url('resetpassword/' . $data->email . '/' . urlencode($code));
            $url = str_replace('public/','', $url); 
            return "<h2>Reset Your Password</h2><p>To change your password <a href='" . str_replace('public/','', $url) . "'>click here.</a></p><p>Or point your browser to this address: <br />" . str_replace('public/','', $url) . "</p> Thank you!</p>";
        }
        if($type == 'report_user'){
            return "<h2>Dear Admins,<br></h2><p>User " . $data['logged_in_user_name'] . " Reported A User <br>User " . $data['username'] . "<br>License Number " . $data['license_number'] . "<br>With Expiry " . $data['expiry'] . "</p><br><h2>Please Respond Accordingly.</h2>";  
        }

        if($type == "onboarding"){
            return  "<h2>Dear ".$data->fname."</h2><p>Thanks for signing up to keep in touch with EVNGO EVENT MANAGEMENT PLATFORM. From now on
            you’ll get regular on upcoming events, special offers and discounted deals. And since you’ll be the first
            to know about the discounts and offers that you can avail while creating new events or attracting
            attendees.<br><br>Now you can create public or private events, increase event awareness, manage events promotions, sell
            tickets and engage attendees all in one place.<br><br>Regards,<br><br><b>EVNGO,<br><br>Customer Support Representative.</b></p>";
        }
    }

}

