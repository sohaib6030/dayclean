<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use URL;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\File;
class ChatListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $user = User::find($this->to_id);   
        if ($this->avatar == 'placeholder.png') {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $this->avatar);
        } else {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' .$this->id . '/' . $this->avatar);
        }
        $user->avatar = $Avatarurl;
        return [
            'id'  => $this->id ?? '',
            'from_id'  => $this->from_id ?? '',
            'to_id'  => $this->to_id ?? '',
            'body'  => $this->body ?? '',
            'attachment'  => $this->attachment ?? '',
            'seen'  => $this->seen ?? '',
            'created_at'  => $this->created_at ?? '',
            'updated_at'  => $this->updated_at ?? '',
            'user'         => $user ?? '',
        ];
    }
}
