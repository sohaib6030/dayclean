<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\JobApplicantsResource;
use App\Models\Jobs;
use App\Models\User;
use App\Models\SubCategory;
use Carbon\Carbon;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        
        return [
            'id'            => $this->id,
            'description'         => $this->description ?? '',
            'offer'         => $this->offer ?? '',
            'address'         => $this->address ?? '',
            'date'         => Carbon::createFromFormat('Y-m-d', $this->date)->format('D M d Y') ?? '',
            'time'         => $this->time ?? '',
            'posted_ago'   => Carbon::parse($this->created_at)->diffForHumans(),
            'status'         => $this->status ?? '',
            'last_updated_by'         => $this->last_updated_by ?? '',
            'created_by'         => $this->created_by ?? '',
            'created_at'         => $this->created_at ?? '',
            'updated_at'         => $this->updated_at ?? '',
            'category'         => $this->category ?? '',
            'subcategories'         => $this->subcategories ?? '',
            'posted_by'         => new UserResource($this->postedBy) ?? '',
            'job_applications_count' => $this->jobApplicationCount->count() ?? '',
            'job_applicant'   => new JobApplicantsResource($this->applicantDetails) ?? '',
            
        ];
    }
}
