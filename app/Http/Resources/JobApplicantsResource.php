<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Category;
use App\Models\UserCategory;
use App\Models\SubCategory;
use App\Models\UserSubCategory;
use App\Models\JobApplication;
use App\Models\UserEquipement;
use URL;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\File;

class JobApplicantsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        $jobApplications = JobApplication::where(['user_id' => $user->id , 'status' => 'completed'])->get();
        $user->jobsCompleteCount = $jobApplications->count();
        $user->ratings =  collect($jobApplications)->average('ratings');
        $user->ratings = number_format($user->ratings, 1, '.', '');
        $user->reviews = number_format($jobApplications->whereNotNull('reviews')->count('reviews'), 0, '.', '');

        $user->equipments = UserEquipement::where('user_id',$user->id)->get();
        $user->user_category = UserCategory::where('user_id',$user->id)->get();

        $user->user_subcategory = UserSubCategory::where('user_id',$user->id)->get();
        $user->joined_since = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('M Y');
        //check if user has equipments or not
        if(count($user->equipments) > 0){
             $user->has_equipment = true;
        }else{
             $user->has_equipment = false;
        }
        
        if ($this->avatar == 'placeholder.png') {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $this->avatar);
        } else {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' .$this->id . '/' . $this->avatar);
        }
        $user->avatar = $Avatarurl;
        return [
            'id'            => $this->id,
            'status'                    => $this->status ?? '',
            'applicant_details'         => $user ?? '',
        ];
    }
}
