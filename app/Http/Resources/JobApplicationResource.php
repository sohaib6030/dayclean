<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;

class JobApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'user'         => new UserResource($this->user) ?? '',
            'date'         => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('D M d Y') ?? '',
            'time'         => $this->time ?? '',
            'applied_ago'   => Carbon::parse($this->created_at)->diffForHumans(),
            'status'         => $this->status ?? '',
            'last_updated_by'         => $this->last_updated_by ?? '',
            'created_by'         => $this->created_by ?? '',
            'created_at'         => $this->created_at ?? '',
            'updated_at'         => $this->updated_at ?? '',
        ];
    }
}
