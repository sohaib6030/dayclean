<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;

class CategoryController extends Controller
{
    public $successStatus = 200;

    public function getCategories(Request $request){
        
        $data = [];
        if($request->has('category_id')){
            $category = Category::where('id',$request->category_id)->get();
            $sub_category = SubCategory::where('category_id',$request->category_id)->get();
            $data['category'] =  $category;
            $data['sub_category'] =   $sub_category;
        }
        if($request->has('sub_category_id')){
            $sub_category = SubCategory::where('id',$request->sub_category_id)->get();
            $data['category'] = [];
            $data['sub_category'] =  $sub_category;
           
        }
        if(!isset($request->category_id) && !isset($request->sub_category_id)){
            $categories = Category::all();
            $sub_categories = SubCategory::all();
            $data['categories'] =  $categories;
            $data['sub_categories'] =  $sub_categories;
        }
        
        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'success',
                'data' => $data
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No categories found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }
}
