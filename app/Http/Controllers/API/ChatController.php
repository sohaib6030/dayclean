<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ChatListResource;
use App\Models\Chat;
use Carbon\Carbon;
use Auth;

class ChatController extends Controller
{
    public $successStatus = 200;

    public function getChats(Request $request){
        $user = Auth::user();
        $ids = Chat::where(['from_id' => $user->id])->orderBy('created_at','desc')->pluck('to_id');
        // dd($ids->toArray());
        // return array_unique($ids->toArray());
        
        $chat = Chat::where(['from_id' => $user->id])->orderBy('created_at','desc')->with('user')->get();
        
        // dd($chat);
        if($chat){
           
            $response_data = [
                'success' => 1,
                'message' => 'Chat List!',
                'data' => ChatListResource::collection($chat)
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'Chat Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
}
