<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\JobResource;
use App\Http\Resources\JobApplicantsResource;
use App\Http\Resources\UserResource;
use App\Models\Jobs;
use App\Models\User;
use App\Models\PostJobCategory;
use App\Models\JobApplication;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Validator;
use Carbon\Carbon;
use Auth;
use DB;
use URL;

class JobController extends Controller
{

    public $successStatus = 200;

    public function getJobs(Request $request){
        $user = Auth::user();
        $status = $request->status;
       
        $jobs = Jobs::where('status',$status)->where('posted_by', '!=' , $user->id)->orderBy('created_at','desc')->get();
        if(count($jobs) > 0){
                $response_data = [
                    'success' => 1,
                    'message' => 'Jobs List',
                    'data' => JobResource::collection($jobs),'job_count' => $jobs->count()
                ];
        
                return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Jobs Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        
    }

    public function myJobs(Request $request){
        $validator = Validator::make($request->all(), [
            'status'      => 'required'     
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => false,
                'message' => 'Incomplete data provided!',
                'errors' => $validator->errors()
            ];
            return response()->json($response_data);
        }
        $user = Auth::user();
        $status = $request->status;
        if($status){
                $jobs = Jobs::where(['posted_by' =>  $user->id ,'status' => $status])->orderBy('created_at','desc')->get();
            
        }
        if(count($jobs) > 0){
            $response_data = [
                'success' => 1,
                'message' => 'Jobs List',
                'data' => JobResource::collection($jobs)
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Jobs Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }

    public function createJob(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'description'         => 'required',
            'offer'         => 'required',
            'address'         => 'required',
            'category_id'         => 'required|integer',
            'sub_category_id'      => 'required'     
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => false,
                'message' => 'Incomplete data provided!',
                'errors' => $validator->errors()
            ];
            return response()->json($response_data);
        }
        
        
        $job = Jobs::create([
            'description'         => $request->description,
            'offer'         => $request->offer,
            'address'         => $request->address,
            'posted_by'         => $user->id,
            'category_id'         => $request->category_id,
            'date'      =>  Carbon::createFromFormat('Y-m-d', $request->date)->format('Y-m-d'),
            'time'      => $request->time,
            'status'  => 'active',
            'last_updated_by' => '',
            'created_by' => $user->id,

        ]);
        if(isset($request->sub_category_id)){
            foreach($request->sub_category_id as $subCatId){
                PostJobCategory::create(['job_id' => $job->id, 'sub_category_id' => $subCatId]);
            } 
        }
        
        if($job){

            $response_data = [
                'success' => true,
                'message' => 'Job has been created!',
                'data' => new JobResource($job)
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => false,
                'message' => 'Error while creating job post!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function jobActive(Request $request){
        $user = Auth::user();
        $jobId = $request->job_id;
        
        if($jobId){
                $job =   Jobs::where(['id' =>  $jobId])->update(['status' => 'active']);
        }
        if($job){
            $response_data = [
                'success' => 1,
                'message' => 'Job '.$status. ' successfully!'
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Job Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }

    public function jobCancel(Request $request){
        $user = Auth::user();
        $jobId = $request->job_id;
        
        $inprogressJob = JobApplication::where('job_id' ,$jobId)->where('status' ,'=' ,'inprogress')->exists();

        if($inprogressJob == true){
            $response_data = [
                'success' => 1,
                'message' => 'You cannot cancel job while in progress!'
            ];
    
            return response()->json($response_data, $this->successStatus);
        }      
        $jobUpdate = Jobs::where('id' ,$jobId)->update(['status' => 'cancelled']);
        
        if($jobUpdate == true){
            JobApplication::where('job_id' ,$jobId)->update(['status' => 'cancelled']);
            $response_data = [
                'success' => 1,
                'message' => 'Job cancelled successfully!'
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Job Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function jobComplete(Request $request){

        $validator = Validator::make($request->all(), [
            'ratings'         => 'required|integer|between:1,5',
            'reference'         => 'required',
            'user_id'         => 'required|integer',   
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => false,
                'message' => 'Incomplete data provided!',
                'errors' => $validator->errors()
            ];
            return response()->json($response_data);
        }
        $jobId = $request->job_id;
        
        $jobUpdate =   Jobs::where(['id' =>  $jobId])->update(['status' => 'completed']);
        if($jobUpdate == true){

         $invoiceUrl = URL::to('/') . Storage::disk('local')->url('public/invoice/test.pdf');
        
         $applicationUpdate = JobApplication::where(
                            [
                                'job_id' => $jobId ,
                                'user_id' => $request->user_id ,
                                'status' => 'active'  //if job is active while in progress
                             ])
                            ->update([
                                'reviews' => $request->reviews,
                                'ratings' => $request->ratings,
                                'invoice' => $invoiceUrl,
                                'reference' => $request->reference,
                                'status' => 'completed'
                            ]);
        }
        
        if($applicationUpdate == true){
            $response_data = [
                'success' => 1,
                'message' => 'Job completed successfully!',
                'data' => ''
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Job Application Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function getJobApplicants(Request $request){

        $jobId = $request->job_id;
        $applications = JobApplication::where('job_id' ,$jobId)->get();
        
        if($applications){
            $response_data = [
                'success' => 1,
                'message' => 'Applicants list!',
                'data' =>  JobApplicantsResource::collection($applications)
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Job Application Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function jobAccept(Request $request)
    {

        $user = $request->user_id;
        $jobId = $request->job_id;
        
        $inprogressJob = JobApplication::where(['job_id'  => $jobId , 'user_id' => $user])
                                        ->where('status' ,'=' ,'pending')->exists();
        if($inprogressJob == true){
            $jobUpdate = Jobs::where('id' ,$jobId)->update(['status' => 'inprogress']);
            JobApplication::where(['job_id' => $jobId , 'user_id' => $user])->update(['status' => 'active']);
            $response_data = [
                'success' => 1,
                'message' => 'Job application accepted successfully!',
                'data' => []
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No Job Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
}
