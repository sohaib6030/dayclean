<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\JobApplication;
use App\Http\Resources\JobApplicationResource;
use Validator;
use Auth;

class JobApplicationController extends Controller
{
    public $successStatus = 200;

    public function createJobApplication(Request $request){

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'job_id'         => 'required'  
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => false,
                'message' => 'Incomplete data provided!',
                'errors' => $validator->errors()
            ];
            return response()->json($response_data);
        }
        $applicationExists = JobApplication::where(['job_id' => $request->job_id , 'user_id' => $user->id])->exists();
        if($applicationExists == true){

            $response_data = [
                'success' => true,
                'message' => 'You have already applied for this job!'
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        $jobApplication = JobApplication::create([
            'job_id'         => $request->job_id,
            'user_id'         => $user->id,
            'reviews'         => $request->reviews,
            'ratings'         => $request->ratings,
            'invoice'         => $request->invoice,
            'status'  =>        $request->status,
            'last_updated_by' => $user->id,
            'created_by' => $user->id

        ]);
      
        
        if($jobApplication){

            $response_data = [
                'success' => true,
                'message' => 'Job application has been created!',
                'data' => new JobApplicationResource($jobApplication)
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => false,
                'message' => 'Error while creating job application!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function directHire(Request $request){

        $userId = $request->user_id;
        $jobId = $request->job_id;
        $status = $request->status;

        $validator = Validator::make($request->all(), [
            'job_id'         => 'required',
            'user_id'         => 'required',  
            'status'         => 'required'  
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => false,
                'message' => 'Incomplete data provided!',
                'errors' => $validator->errors()
            ];
            return response()->json($response_data);
        }
        $applicationExists = JobApplication::where(['job_id' => $request->job_id , 'user_id' => $userId])->exists();
        if($applicationExists == true){

            $response_data = [
                'success' => true,
                'message' => 'Application already exist with this user!'
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        $jobApplication = JobApplication::create([
            'job_id'         => $jobId,
            'user_id'         => $userId,
            'reviews'         => $request->reviews,
            'ratings'         => $request->ratings,
            'invoice'         => $request->invoice,
            'status'  =>        $request->status,
            'last_updated_by' => $userId,
            'created_by' => $userId

        ]);
      
        $jobUpdate =   Jobs::where(['id' =>  $jobId])->update(['status' => 'inprogress']);
        if($jobUpdate){

            $response_data = [
                'success' => true,
                'message' => 'You have successfully hired user!',
                'data' => new JobApplicationResource($jobApplication)
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => false,
                'message' => 'Error while creating job application!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
}
