<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\CategoryProfileResource;
use App\Models\User;
use App\Models\Category;
use App\Models\UserCategory;
use App\Models\SubCategory;
use App\Models\UserSubCategory;
use App\Models\JobApplication;
use App\Models\UserEquipement;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Str;
use App\Http\Helpers\Helper;
use URL;
use DB;
use Carbon\Carbon;

class ProfileController extends Controller
{

    public $successStatus = 200;

    public function profileFetch(Request $request){
        $data = [];
        $user = User::find(Auth::user()->id);

        $jobApplications = JobApplication::where(['user_id' => $user->id , 'status' => 'completed'])->get();
        $user->jobsCompleteCount = $jobApplications->count();
        $user->ratings =  collect($jobApplications)->average('ratings');
        $user->ratings = number_format($user->ratings, 1, '.', '');
        $user->reviews = number_format($jobApplications->whereNotNull('reviews')->count('reviews'), 0, '.', '');

        $user->equipments = UserEquipement::where('user_id',$user->id)->get();
        $user->user_category = UserCategory::where('user_id',$user->id)->get();

        $user->user_subcategory = UserSubCategory::where('user_id',$user->id)->get();
        $user->joined_since = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('M Y');
        //check if user has equipments or not
        if(count($user->equipments) > 0){
             $user->has_equipment = true;
        }else{
             $user->has_equipment = false;
        }
        
        if ($user->avatar == 'placeholder.png') {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->avatar);
        } else {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->id . '/' . $user->avatar);
        }
        $user->avatar = $Avatarurl;
        
        if($user){
            $response_data = [
                'success' => 1,
                'message' => 'user found!',
                'data' => $user
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => 0,
                'message' => 'No User Found!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function updateProfile(Request $request){

        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'firstname'         => 'required',
            'lastname'         => 'required',
            'country'         => 'required',
            'city'         => 'required',
            'address'         => 'required',
            'email'         => 'required|email',
            'contact'         => 'required',
            'category_id'         => 'required',
            'sub_category_id'      => 'required',     
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => false,
                'message' => 'Incomplete data provided!',
                'errors' => $validator->errors()
            ];
            return response()->json($response_data);
        }

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->name = ucfirst($request->firstname) . ' '.ucfirst($request->lastname) ;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->contact = $request->contact;
        $user->profile_description = $request->profile_description;
        $user->save();

        //update user categories
        UserCategory::where('user_id',$user->id)->delete();
        if($request->has('category_id')){   
            foreach($request->category_id as $cat_id){
                $categoryName = Category::where('id',$cat_id)->first();
                if($categoryName){
                UserCategory::create(['id' => $cat_id,'user_id' => $user->id,'category_id'=> $cat_id,'name' => $categoryName->name,'last_updated_by' => $user->id,'created_by' => $user->id]);
                }
            }
        }
         //update user categories
         UserSubCategory::where('user_id',$user->id)->delete();
         if($request->has('sub_category_id')){
            UserSubCategory::where('user_id',$user->id)->delete();
            foreach($request->sub_category_id as $sub_cat_id){
                $subcategoryName = SubCategory::where('id',$sub_cat_id)->first();
                if($subcategoryName){
                    UserSubCategory::create(['id' => $sub_cat_id,'user_id' => $user->id,'sub_category_id'=> $sub_cat_id,'name' => $subcategoryName->name,'last_updated_by' => $user->id,'created_by' => $user->id]);
                }
                
            }
        }

          //update user equipments
        UserEquipement::where('user_id',$user->id)->delete();  
        if($request->has('equipments')){
                
                foreach($request->equipments as $equip){
                    UserEquipement::create(['user_id' => $user->id,'name'=> $equip]);
                }
        }
        

        //add user avatar
        if ($request->hasfile('avatar')) {
           
            $file = $request->file('avatar');
            $avatar = Str::random(20) . ".png";
            Storage::disk('local')->put('/public/users/' . $user->id . '/' . $avatar, File::get($file));
            $user->avatar = $avatar;
            $user->save();
        }
        if ($user->avatar == 'placeholder.png') {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->avatar);
        } else {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->id . '/' . $user->avatar);
        }

        $user->avatar = $Avatarurl;
        if($user){
            $response_data = [
                'success' => true,
                'message' => 'Profile Update Successfully!',
                'data' => $user
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => false,
                'message' => 'Error while updating profile!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function getCategoriesProfiles(Request $request){
        $categoryId = $request->category_id;
        $users = UserCategory::where('category_id' , $categoryId)->with('users')->get();
        if($users){
            $response_data = [
                'success' => true,
                'message' => 'Profiles List!',
                'data' => CategoryProfileResource::collection($users)
            ];
    
            return response()->json($response_data, $this->successStatus);
        }
        else {
            $response_data = [
                'success' => false,
                'message' => 'Error while updating profile!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
}
