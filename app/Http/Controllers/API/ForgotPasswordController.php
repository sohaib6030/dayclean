<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use App\Http\Helpers\Helper;

class ForgotPasswordController extends Controller
{
    public $successStatus = 200;

    public function ForgotPassword(Request $request){

        $email = $request->email;
        $data =  User::where('email',$email)->first();
        if($data)
        {
            $token = Password::getRepository()->create($data);
            $code = $token;
           // $data->code = $code;
            $data->save();
            Helper::sendCustomEmail($data,'forgotpassword',12345);


            $response_data = [
                'success' => 1,
                'message' => 'Account password reset successfully.Please check your email account and follow the instruction!',
            ];

            return  response()->json($response_data, $this->successStatus);
        }else{
            $response_data = [
                'success' => 0,
                'message' => 'This email address does not exist!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
}
