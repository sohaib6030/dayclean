<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\JobResource;
use App\Http\Resources\CitiesCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Jobs;
use App\Models\Chat;
use App\Models\JobApplication;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\UserCategory;
use App\Models\UserSubCategory;
use App\Models\Cities;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Helper;
use Notification;
use App\Notifications\EmailVerification;
use URL;
use Validator;
use Mail;

class UserController extends Controller
{
    

    public $successStatus = 200;

    public function test(Request $request){
        return new JobResource(auth()->user());
    }

    public function login(Request $request){
    
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();
            if($user->status=='active' && $user->email_verified_at != '' && $user->verify == 1){
                
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $id = auth()->user()->id;
                $token = $success['token'];
        
                $response_data = [
                    'success' => 1,
                    'message' => 'Login success.',
                    'data' => $success,
                    'user' => new UserResource($user)
                ];

                return response()->json($response_data, $this->successStatus);

            }else {
                $response_data = [
                    'success' => 2,
                    'message' => 'Your account is not verified.Please check email and verify your account'
                ];
                return response()->json($response_data,  $this->successStatus);
            }

        } else {
            $response_data = [
                'success' => 0,
                'message' => 'Invalid Email or Password, please try again.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'         => 'required',
            'lastname'         => 'required',
            'email'         => 'required|email|unique:users',
            'password'      => 'required',
            'confirm_password'    => 'required|same:password'
        ]);


        if( $request->password != $request->confirm_password )
        {
            $response_data = [
                'success' => 0,
                'message' => 'password and confirm password mismatched.',
                'email' => '',
                'user' => null,
            ];
            return response()->json($response_data);
        }

        $email = '';
        if ($validator->fails()) {
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                foreach ($messages as $message) {
                    if ($field_name == 'email') {
                        $email = $message;
                    }

                }
            }
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'email' => ($email != '') ? $email : 'incomplete data provided',
                'user' => null,

            ];
            return response()->json($response_data);
        }


        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['name'] = $input['firstname'] . ' ' . $input['lastname'] ;
        $input['firstname']  = $input['firstname'] ;
        $input['lastname']  = $input['lastname'] ;
        $input['status'] = 'active';
        $user = User::create($input);
        $data = User::find($user->id);
        $data->name = $input['name'];
        $code = rand(999, 99999);
        $data->verify_code = $code;
        $data->verify = true;
        $data->email_verified_at = date('Y-m-d H:i:s');
        $data->save();

        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $url = url('verify/account/'.$data->email.'/'.urlencode($code));
        $details = [
            'greeting' => 'Hi, '.$user->name,
            'body' => 'click the link below to verify your email',
            'url' => $url,
            'thanks' => 'thank you'
        ];

        //send email verify to user email
        try {
            Notification::send($user, new EmailVerification($details));
          } catch (\Exception $e) {
          
              $email_message =  $e->getMessage();
          }
        $response_data = [
            'success' => 1,
            'message' =>  $data->name. ' your
             registration has been successful please check you email and
             verify your account (if you cannot find this email please check
             your junk mail)',
            'email_message' =>  isset($email_message) ? $email_message : '',
            'user' => new UserResource($data),
        ];
        return response()->json($response_data, $this->successStatus);
    }

    public function verify($email, $code)
    {
        $data =  User::where('email',$email)->first();
        if($data->verify == 1){
            return "Already verified";
        }
        if(isset($data->verify_code)  && $data->verify_code==$code){
            $data->verify = '1';
            $data->email_verified_at = date('Y-m-d H:i:s');
            $data->save();
            return "Successfully Verified";
        }else{
            return "Something went wrong please contact to customer support";
        }

    }

 public function cities(Request $request)
    {
        //$CountryCode = $request->code;

        $CountryCode = ($request->code != null || $request->code != "" ? $request->code : "");
        $CountryName = ($request->countryname != null || $request->countryname != "" ? $request->countryname : "");
        $list="";
        $countrylist="";

        if($CountryCode == "")
        {
            $list = Cities::where('is_deleted','0')->pluck('CountryCode');
        }
        else
        {
            $list= Cities::where('is_deleted','0')->where('CountryCode',$CountryCode)->pluck('CountryCode');
        }

        if($CountryName == "")
        {
            $countrylist = Cities::where('is_deleted','0')->pluck('countryName');
        }
        else
        {
            $countrylist= Cities::where('is_deleted','0')->where('countryName',$CountryName)->pluck('countryName');
        }

        $data = Cities::where('status','Active')->where('is_deleted','0')->whereIn('CountryCode',$list)->whereIn('countryName',$countrylist)->orderBy('id','asc')->get();

        if(count($data)>0){

            return new CitiesCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
}

