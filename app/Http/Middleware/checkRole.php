<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class checkRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,... $guards)
    {
        $user = Auth::user();
        if($user->isSuperAdmin() == false){
            return 'Access not allowed';
        }
            

        return $next($request);
    }
}
