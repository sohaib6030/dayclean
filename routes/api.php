<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('GetCities', [App\Http\Controllers\API\UserController::class, 'cities']);
Route::post('login', [App\Http\Controllers\API\UserController::class, 'login']);
Route::post('register', [App\Http\Controllers\API\UserController::class, 'register']);
Route::post('forgot-password', [App\Http\Controllers\API\ForgotPasswordController::class, 'forgotPassword']);
Route::post('change-password', [App\Http\Controllers\API\ForgotPasswordController::class, 'changePassword']);

// Authenticated api with bearer-token
Route::group(['middleware' => 'auth:api'], function(){

    //users & Profile
    Route::get('users',[App\Http\Controllers\API\UserController::class, 'getUsers']);
    Route::post('users/create',[App\Http\Controllers\API\UserController::class, 'createUser']);
    Route::post('users/update/{id}',[App\Http\Controllers\API\UserController::class, 'updateUser']);
    Route::post('users/delete/{id}',[App\Http\Controllers\API\UserController::class, 'deleteUser']);
    Route::post('profile',[App\Http\Controllers\API\ProfileController::class, 'profileFetch']);
    Route::post('category/profiles',[App\Http\Controllers\API\ProfileController::class, 'getCategoriesProfiles']);
    Route::post('profile/update',[App\Http\Controllers\API\ProfileController::class, 'updateProfile']);
    

    //categories and sub-categories
    Route::get('category',[App\Http\Controllers\API\CategoryController::class, 'getCategories']);
    Route::post('category/create',[App\Http\Controllers\API\CategoryController::class, 'createCategory']);
    Route::get('category/update/{id}',[App\Http\Controllers\API\CategoryController::class, 'updateCategory']);
    Route::get('category/delete/{id}',[App\Http\Controllers\API\CategoryController::class, 'deleteCategory']);
    
    //jobs
    Route::get('jobs',[App\Http\Controllers\API\JobController::class, 'getJobs']);
    Route::post('my-jobs',[App\Http\Controllers\API\JobController::class, 'myJobs']);
    Route::post('job-applicants',[App\Http\Controllers\API\JobController::class, 'getJobApplicants']);
    Route::post('jobs-search',[App\Http\Controllers\API\JobController::class, 'searchJobs']);
    Route::post('job/create',[App\Http\Controllers\API\JobController::class, 'createJob']);
    Route::post('job/update',[App\Http\Controllers\API\JobController::class, 'updateJob']);
    Route::post('job/active',[App\Http\Controllers\API\JobController::class, 'jobActive']);
    Route::post('job/cancel',[App\Http\Controllers\API\JobController::class, 'jobCancel']);
    Route::get('job/accept',[App\Http\Controllers\API\JobController::class, 'jobAccept']);
    Route::post('job/complete',[App\Http\Controllers\API\JobController::class, 'jobComplete']);
    Route::get('job/delete/{id}',[App\Http\Controllers\API\JobController::class, 'deleteJob']);
    
    //jobs_application
    Route::get('job-application',[App\Http\Controllers\API\JobApplicationController::class, 'getJobApplications']);
    Route::post('job-application/create',[App\Http\Controllers\API\JobApplicationController::class, 'createJobApplication']);
    Route::post('job-application/hire',[App\Http\Controllers\API\JobApplicationController::class, 'directHire']);
    Route::get('job-application/update/{id}',[App\Http\Controllers\API\JobApplicationController::class, 'updateJobApplication']);
    Route::get('job-application/delete/{id}',[App\Http\Controllers\API\JobApplicationController::class, 'deleteJobApplication']);
    
    //chats
    Route::post('chat',[App\Http\Controllers\API\ChatController::class, 'getChats']);
    Route::post('chat/create',[App\Http\Controllers\API\ChatController::class, 'createChat']);
    Route::get('chat/update/{id}',[App\Http\Controllers\API\ChatController::class, 'updateChat']);
    Route::get('chat/delete/{id}',[App\Http\Controllers\API\ChatController::class, 'deleteChat']);

});


