<?php

use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function(){
    return view('welcome');
});
Auth::routes();

Route::get('verify/account/{email}/{code}', [App\Http\Controllers\API\UserController::class , 'verify']);


